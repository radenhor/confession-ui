//
//  WelcomeVC.swift
//  ConfessionUI
//
//  Created by Raden on 15/November/20.
//  Copyright © 2020 Raden. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    @IBAction func didTapSignInAction(_ sender: UIButton) {
        let vc = UIStoryboard(name: "SignInSB", bundle: nil).instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapSignUpAction(_ sender: UIButton) {
        let vc = UIStoryboard(name: "SignupSB", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
