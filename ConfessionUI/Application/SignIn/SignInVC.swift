//
//  SignInVC.swift
//  ConfessionUI
//
//  Created by Raden on 21/November/20.
//  Copyright © 2020 Raden. All rights reserved.
//

import UIKit

class SignInVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }

    @IBAction func didTabBackButtonAction(_ sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func didTapSignUpAction(_ sender: UIButton) {
        let vc = UIStoryboard(name: "SignupSB", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapForgetPasswordAction(_ sender: UIButton) {
        let vc = UIStoryboard(name: "ForgetPasswordSB", bundle: nil).instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapSiginButtonAction(_ sender: UIButton) {
        let vc = UIStoryboard(name: "HostSB", bundle: nil).instantiateViewController(withIdentifier: "HostVC") as! HostVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
