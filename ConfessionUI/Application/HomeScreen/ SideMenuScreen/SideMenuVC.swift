//
//  SideMenuVC.swift
//  ConfessionUI
//
//  Created by Raden on 22/November/20.
//  Copyright © 2020 Raden. All rights reserved.
//

import UIKit
import InteractiveSideMenu

class SideMenuVC: MenuViewController, Storyboardable {
    
    // Mark: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    // Mark: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

// Mark: - UITableView
extension SideMenuVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 5 {
            let secondCell = tableView.dequeueReusableCell(withIdentifier: "MenuItemSeperatorTableViewCell", for: indexPath) as! MenuItemSeperatorTableViewCell
            return secondCell
        }
        else {
            let firstCell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTableViewCell", for: indexPath) as! MenuItemTableViewCell
            return firstCell
        }
    }
    
    
}
