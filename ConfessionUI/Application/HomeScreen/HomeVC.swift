//
//  HomeVC.swift
//  ConfessionUI
//
//  Created by Raden on 21/November/20.
//  Copyright © 2020 Raden. All rights reserved.
//

import UIKit
import InteractiveSideMenu

class HomeVC: UIViewController, SideMenuItemContent, Storyboardable {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
     
    @IBAction func didTapMenuAction(_ sender: UIButton) {
        showSideMenu()
    }
    
}
